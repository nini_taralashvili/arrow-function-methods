//#1st task
const names1 = ["George", "Nick", "Tom", "Kate", "Annie"];
const names2 = ["James", "Will", "Jack", "Nate", "Edward"];

myFriends = (friends) => {
    return friends.filter(element => element.length === 4);
}
console.log(myFriends(names1));
console.log(myFriends(names2));

//#2nd task
const nums1 = [5, 8, 12, 19, 22];
const nums2 = [52, 76, 14, 12, 4];
const nums3 = [19, -5, 42, 2, 77];
const nums4 = [3, 87, 45, 12, 7];

 sorted = (numbers) => {
     let arr = numbers.sort((n1,n2) => n1 - n2 ) && numbers.filter(item => (item > 0) && (item%1==0));
     return arr[0] + arr[1];
 }
 
 console.log(sorted(nums1));
 console.log(sorted(nums2));
 console.log(sorted(nums3));
 console.log(sorted(nums4));
